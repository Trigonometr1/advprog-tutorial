package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    /**
     * Cast method for a Spell
     * @param spellName search the name, then retrieve the Spell
     */
    public void castSpell(String spellName) {
        // TODO: Complete Me
        this.latestSpell = this.spells.get(spellName);
        this.latestSpell.cast();
    }

    /**
     * Undo the Spell casted recently
     */
    public void undoSpell() {
        // TODO: Complete Me
        if (this.latestSpell != null) { latestSpell.undo(); }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
