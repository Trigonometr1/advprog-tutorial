package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

public interface Spell {
    // Interface Command
    /**
     * This will call cast method from Familiar or HighSpirit
     */
    public void cast();

    /**
     * This will call undo method from Familiar or HighSpirit
     */
    public void undo();

    /**
     * To identify the name of the class
     * @return the name of spell or the class
     */
    public String spellName();
}
