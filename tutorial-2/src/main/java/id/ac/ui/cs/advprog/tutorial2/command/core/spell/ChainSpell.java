package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    /**
     * This will call cast method from Familiar or HighSpirit
     */
    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    /**
     * This will call undo method from Familiar or HighSpirit
     */
    @Override
    public void undo() {
        // Urutannya terbalik
        for (int i = this.spells.size() - 1; i >= 0; i--) {
            this.spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
