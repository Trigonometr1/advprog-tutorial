package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {
    // TODO: Complete Me

    /**
     * Constructor for Familiar's Seal Spell
     * @param familiar associated with the spell
     */
    public FamiliarSealSpell(Familiar familiar) {
        super(familiar);
    }

    /**
     * This will call seal method from Familiar
     */
    @Override
    public void cast() {
        this.familiar.seal();
    }

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
