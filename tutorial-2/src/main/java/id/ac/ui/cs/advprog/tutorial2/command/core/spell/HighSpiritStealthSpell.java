package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {
    // TODO: Complete Me

    public HighSpiritStealthSpell(HighSpirit spirit) {
        super(spirit);
    }

    /**
     * This will call stealth method from HighSpirit
     */
    @Override
    public void cast() {
        this.spirit.stealthStance();
    }

    /**
     * To identify the name of the class
     *
     * @return the name of spell or the class
     */
    @Override
    public String spellName() {
        return this.spirit.getRace() + ":Stealth";
    }

}
