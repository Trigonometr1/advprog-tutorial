package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    /**
     * Executed when there are changes in class Adventurer
     */
    @Override
    public void update() {
        // Knight bisa semua quest
        this.getQuests().add(this.guild.getQuest());
    }
}
