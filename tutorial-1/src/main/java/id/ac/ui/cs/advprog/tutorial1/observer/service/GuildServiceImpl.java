package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();
        //ToDo: Complete Me
        // Create new object, then put it inside the guild
        this.agileAdventurer = new AgileAdventurer(this.guild);
        this.guild.add(agileAdventurer);

        this.knightAdventurer = new KnightAdventurer(this.guild);
        this.guild.add(knightAdventurer);

        this.mysticAdventurer = new MysticAdventurer(this.guild);
        this.guild.add(mysticAdventurer);
    }

    //ToDo: Complete Me

    /**
     * Add Quest to QuestRepository
     *
     * @param quest to be added in QuestRepository
     */
    @Override
    public void addQuest(Quest quest) {
        if (this.questRepository.save(quest) != null) {
            // Kalau yang direturn bukan null, sudah ada objectnya
            // aman dikasih ke Guild objek Questnya
            this.guild.addQuest(quest);
        }
    }

    /**
     * Get list of Adventurers in the Guild
     *
     * @return List of Adventurers
     */
    @Override
    public List<Adventurer> getAdventurers() {
        return this.guild.getAdventurers();
    }
}
