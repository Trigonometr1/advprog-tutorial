package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    //ToDo: Complete me

    public KnightAdventurer() {
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    /**
     * Instead of the class's name, you can switch it with its alias to be more familiar.
     *
     * @return alias String for particular Adventurer.
     */
    @Override
    public String getAlias() {
        return "Give way to the Knights!";
    }


}
