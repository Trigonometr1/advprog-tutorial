package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me

    /**
     * When this method executed, Adventurer attacking.
     *
     * @return String that indicates the attack behavior.
     */
    @Override
    public String attack() {
        return "Menyerang dengan Magic";
    }

    /**
     * Explained what Strategy type Adventurer used.
     *
     * @return String, e.g the name of the class that implements this method
     */
    @Override
    public String getType() {
        return "AttackWithMagic";
    }
}
