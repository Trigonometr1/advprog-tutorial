package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    /**
     * Executed when there are changes in class Adventurer
     */
    @Override
    public void update() {
        // There are Delivery (D), Rumble (R), and Escort (E)
        // Agile cuma bisa D ama R, jangan jalanin yang E
        if(this.guild.getQuestType().equals("E")) { return; }

        this.getQuests().add(this.guild.getQuest());
    }
}
