package id.ac.ui.cs.advprog.tutorial1.strategy.service;

import id.ac.ui.cs.advprog.tutorial1.strategy.core.*;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;
import id.ac.ui.cs.advprog.tutorial1.strategy.repository.StrategyRepository;
import org.springframework.stereotype.Service;

@Service
public class AdventurerServiceImpl implements AdventurerService {

    private final AdventurerRepository adventurerRepository;

    private final StrategyRepository strategyRepository;


    public AdventurerServiceImpl(
            AdventurerRepository adventurerRepository,
            StrategyRepository strategyRepository) {

        this.adventurerRepository = adventurerRepository;
        this.strategyRepository = strategyRepository;
    }

    @Override
    public Iterable<Adventurer> findAll() {
        return adventurerRepository.findAll();
    }

    @Override
    public Adventurer findByAlias(String alias) {
        return adventurerRepository.findByAlias(alias);
    }

    /**
     * Change Strategy type (Attack and Defense) for particular Adventurer
     *
     * @param alias       Identifier needed for the adventurer
     * @param attackType  The AttackBehavior will be changed to this
     * @param defenseType The DefenseBehavior will be changed to this
     */
    @Override
    public void changeStrategy(String alias, String attackType, String defenseType) {
        //ToDo: Complete me

        // Get Adventurer particular with the aliases
        Adventurer adventurer = this.findByAlias(alias);

        // Get AttackBehavior and DefenseBehavior
        AttackBehavior attackBehavior = strategyRepository.getAttackBehaviorByType(attackType);
        DefenseBehavior defenseBehavior = strategyRepository.getDefenseBehaviorByType(defenseType);

        // Set it to the adventurer
        adventurer.setAttackBehavior(attackBehavior);
        adventurer.setDefenseBehavior(defenseBehavior);
    }

    @Override
    public Iterable<AttackBehavior> getAttackBehaviors() {
        return strategyRepository.getAttackBehaviors();
    }

    @Override
    public Iterable<DefenseBehavior> getDefenseBehaviors() {
        return strategyRepository.getDefenseBehaviors();
    }
}
