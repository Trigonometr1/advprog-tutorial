package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    //ToDo: Complete me

    public MysticAdventurer() {
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    /**
     * Instead of the class's name, you can switch it with its alias to be more familiar.
     *
     * @return alias String for particular Adventurer.
     */
    @Override
    public String getAlias() {
        return "Magic thyself out of that!";
    }
}
