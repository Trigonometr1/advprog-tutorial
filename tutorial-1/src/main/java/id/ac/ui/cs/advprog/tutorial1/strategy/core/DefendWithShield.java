package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me

    /**
     * When this method executed, Adventurer defending
     *
     * @return String that indicates the defend behavior
     */
    @Override
    public String defend() {
        return "Bertahan dengan Shield";
    }

    /**
     * Explained what Strategy type Adventurer used.
     *
     * @return String, e.g the name of the class that implements this method
     */
    @Override
    public String getType() {
        return "DefendWithShield";
    }
}
