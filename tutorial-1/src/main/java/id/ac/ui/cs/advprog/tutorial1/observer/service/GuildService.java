package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Quest;

import java.util.List;

public interface GuildService {
    /**
     * Add Quest to QuestRepository
     * @param quest to be added in QuestRepository
     */
    void addQuest(Quest quest);

    /**
     * Get list of Adventurers in the Guild
     * @return List of Adventurers
     */
    List<Adventurer> getAdventurers();
}
