package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public interface AttackBehavior extends Strategy {

    /**
     * When this method executed, Adventurer attacking.
     *
     * @return String that indicates the attack behavior.
     */
    String attack();
}
