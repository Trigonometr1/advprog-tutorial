package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public interface DefenseBehavior extends Strategy {

    /**
     * When this method executed, Adventurer defending
     *
     * @return String that indicates the defend behavior
     */
    String defend();
}
