package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    /**
     * Executed when there are changes in class Adventurer
     */
    @Override
    public void update() {
        // Mystic cuma ngerjain Delivery dan Escort, jangan terima Rumble
        if(this.guild.getQuestType().equals("R")){ return; }

        this.getQuests().add(this.guild.getQuest());
    }
}
