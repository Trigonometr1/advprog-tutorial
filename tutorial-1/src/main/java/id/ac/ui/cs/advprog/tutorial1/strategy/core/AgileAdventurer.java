package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
	
    //ToDo: Complete me

    public AgileAdventurer() {
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    /**
     * Instead of the class's name, you can switch it with its alias to be more familiar.
     *
     * @return alias String for particular Adventurer.
     */
    @Override
    public String getAlias() {
        return "Agility is mine!";
    }

}
