package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public interface Strategy {

    /**
     * Explained what Strategy type Adventurer used.
     *
     * @return String, e.g the name of the class that implements this method
     */
    String getType();
}
