Perbedaan _lazy instantiation_ dan _eager instantiation_ adalah pada pembuatan objeknya.
_Lazy instantiation_ akan melakukan _instantiation_ ketika dipanggil saja, sementara
_eager instantiation_ akan selalu melakukan _instantiation_ walaupun tidak ada pemanggilan.

_Eager instantiation_ memiliki kelebihan ketika ada banyak _thread_ yang akan menggunakan
objek _singleton_ tersebut. Karena sudah melakukan instansiasi dari awal, maka akan menghemat
waktu yang diperlukan. Namun, kekurangannya akan terasa apabila kita menggunakannya di
lingkungan yang tidak terlalu sering melakukan referensi ke objek _singleton_. Pembuatan
objek ini hanya akan membuat boros memori.

Bisa dikatakan _lazy instantiation_ merupakan kebalikan dari _eager instantiation_,
termasuk pula kelebihan dan kekurangannya. _Lazy instantiation_ dapat digunakan pada lingkungan
yang sulit digunakan oleh _eager instantiation_. Penggunaan memori dan waktu dalam
lingkungan yang tidak terlalu sering melakukan referensi dapat dihemat. Namun, ketika nanti
akan sering direferensikan, kita akan lebih banyak menggunakan waktu dan memori karena kita
akan selalu masuk kedalam kode if yang mengecek _instance_ nya null atau bukan.
