package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class FactoryTest {
    private Class<?> factoryClass;

    @BeforeEach
    public void setup() throws Exception {
        factoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factories.Factory");
    }

    @Test
    public void testFactoryIsAPublicInterface() {
        int classModifiers = factoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testFactoryHasGetFlavorAbstractMethod() throws Exception {
        Method getName = factoryClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testFactoryHasGetMeatAbstractMethod() throws Exception {
        Method getName = factoryClass.getDeclaredMethod("getMeat");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testFactoryHasGetNoodleAbstractMethod() throws Exception {
        Method getName = factoryClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

    @Test
    public void testFactoryHasGetToppingAbstractMethod() throws Exception {
        Method getName = factoryClass.getDeclaredMethod("getTopping");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getName.getParameterCount());
    }

}
