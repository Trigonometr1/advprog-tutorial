package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {
    Udon udon;

    @BeforeEach
    public void setup() throws Exception {
        udon = new Udon();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Mondo Udon Noodles...";
        assertEquals(result, udon.getDescription());
    }
}
