package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {
    Soba soba;

    @BeforeEach
    public void setup() throws Exception {
        soba = new Soba();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Liyuan Soba Noodles...";
        assertEquals(result, soba.getDescription());
    }
}
