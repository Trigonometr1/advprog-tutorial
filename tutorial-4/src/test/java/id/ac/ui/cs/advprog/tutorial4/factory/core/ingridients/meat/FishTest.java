package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    Fish fish;

    @BeforeEach
    public void setup() throws Exception {
        fish = new Fish();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Zhangyun Salmon Fish Meat...";
        assertEquals(result, fish.getDescription());
    }
}
