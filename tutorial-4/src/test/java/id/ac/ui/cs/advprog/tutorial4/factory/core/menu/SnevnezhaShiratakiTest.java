package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki snevnezhaShirataki;
    private final String NAME = "Test Snevnezha Shirataki";

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShirataki = new SnevnezhaShirataki(NAME);
    }

    @Test
    public void testGetNameMethod() {
        assertEquals(NAME, snevnezhaShirataki.getName());
    }

    @Test
    public void testGetNoodleReturnCorrectInstance() {
        Noodle noodle = snevnezhaShirataki.getNoodle();
        assertTrue(noodle instanceof Shirataki);
    }

    @Test
    public void testGetMeatReturnCorrectInstance() {
        Meat meat = snevnezhaShirataki.getMeat();
        assertTrue(meat instanceof Fish);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        Topping topping = snevnezhaShirataki.getTopping();
        assertTrue(topping instanceof Flower);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        Flavor flavor = snevnezhaShirataki.getFlavor();
        assertTrue(flavor instanceof Umami);
    }
}
