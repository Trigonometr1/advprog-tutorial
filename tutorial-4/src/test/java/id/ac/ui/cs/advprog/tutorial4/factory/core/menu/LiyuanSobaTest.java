package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    private LiyuanSoba liyuanSoba;
    private final String NAME = "Test Liyuan Soba";

    @BeforeEach
    public void setup() throws Exception {
        liyuanSoba = new LiyuanSoba(NAME);
    }

    @Test
    public void testGetNameMethod() {
        assertEquals(NAME, liyuanSoba.getName());
    }

    @Test
    public void testGetNoodleReturnCorrectInstance() {
        Noodle noodle = liyuanSoba.getNoodle();
        assertTrue(noodle instanceof Soba);
    }

    @Test
    public void testGetMeatReturnCorrectInstance() {
        Meat meat = liyuanSoba.getMeat();
        assertTrue(meat instanceof Beef);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        Topping topping = liyuanSoba.getTopping();
        assertTrue(topping instanceof Mushroom);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        Flavor flavor = liyuanSoba.getFlavor();
        assertTrue(flavor instanceof Sweet);
    }
}
