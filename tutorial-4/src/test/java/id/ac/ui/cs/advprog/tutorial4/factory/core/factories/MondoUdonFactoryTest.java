package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonFactoryTest {
    MondoUdonFactory monuObject;

    @BeforeEach
    public void setup() throws Exception {
        monuObject = new MondoUdonFactory();
    }

    @Test
    public void testGetFlavorMethod() {
        Flavor flavor = monuObject.getFlavor();
        assertTrue(flavor instanceof Salty);
    }

    @Test
    public void testGetMeatMethod() {
        Meat meat = monuObject.getMeat();
        assertTrue(meat instanceof Chicken);
    }

    @Test
    public void testGetNoodleMethod() {
        Noodle noodle = monuObject.getNoodle();
        assertTrue(noodle instanceof Udon);
    }

    @Test
    public void testGetToppingMethod() {
        Topping topping = monuObject.getTopping();
        assertTrue(topping instanceof Cheese);
    }
}
