package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UmamiTest {
    Umami umami;

    @BeforeEach
    public void setup() throws Exception {
        umami = new Umami();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding WanPlus Specialty MSG flavoring...";
        assertEquals(result, umami.getDescription());
    }
}
