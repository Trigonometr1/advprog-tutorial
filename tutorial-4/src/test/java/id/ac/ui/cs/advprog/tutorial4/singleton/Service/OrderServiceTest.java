package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class OrderServiceTest {
    private final String DRINK_NAME = "Test Drink";
    private final String FOOD_NAME = "Test Food";

    OrderServiceImpl orderService = new OrderServiceImpl();

    @Test
    public void testOrderDrinkShouldChangeDrink() {
        orderService.orderADrink(DRINK_NAME);
        String drink = OrderDrink.getInstance().getDrink();
        assertEquals(DRINK_NAME, drink);
    }

    @Test
    public void testOrderFoodShouldChangeFood() {
        orderService.orderAFood(FOOD_NAME);
        String food = OrderFood.getInstance().getFood();
        assertEquals(FOOD_NAME, food);
    }

    @Test
    public void testGetFoodMethod() {
        assertNotNull(orderService.getFood());
    }

    @Test
    public void testGetDrinkMethod() {
        assertNotNull(orderService.getDrink());
    }
}
