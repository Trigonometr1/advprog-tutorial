package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiFactoryTest {
    SnevnezhaShiratakiFactory sneshiObject;

    @BeforeEach
    public void setup() throws Exception {
        sneshiObject = new SnevnezhaShiratakiFactory();
    }

    @Test
    public void testGetFlavorMethod() {
        Flavor flavor = sneshiObject.getFlavor();
        assertTrue(flavor instanceof Umami);
    }

    @Test
    public void testGetMeatMethod() {
        Meat meat = sneshiObject.getMeat();
        assertTrue(meat instanceof Fish);
    }

    @Test
    public void testGetNoodleMethod() {
        Noodle noodle = sneshiObject.getNoodle();
        assertTrue(noodle instanceof Shirataki);
    }

    @Test
    public void testGetToppingMethod() {
        Topping topping = sneshiObject.getTopping();
        assertTrue(topping instanceof Flower);
    }
}
