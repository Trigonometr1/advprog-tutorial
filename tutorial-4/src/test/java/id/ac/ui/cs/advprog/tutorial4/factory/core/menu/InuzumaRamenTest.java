package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenTest {
    private InuzumaRamen inuzumaRamen;
    private final String NAME = "Test Inuzuma Ramen";

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamen = new InuzumaRamen(NAME);
    }

    @Test
    public void testGetNameMethod() {
        assertEquals(NAME, inuzumaRamen.getName());
    }

    @Test
    public void testGetNoodleReturnCorrectInstance() {
        Noodle noodle = inuzumaRamen.getNoodle();
        assertTrue(noodle instanceof Ramen);
    }

    @Test
    public void testGetMeatReturnCorrectInstance() {
        Meat meat = inuzumaRamen.getMeat();
        assertTrue(meat instanceof Pork);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        Topping topping = inuzumaRamen.getTopping();
        assertTrue(topping instanceof BoiledEgg);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        Flavor flavor = inuzumaRamen.getFlavor();
        assertTrue(flavor instanceof Spicy);
    }
}
