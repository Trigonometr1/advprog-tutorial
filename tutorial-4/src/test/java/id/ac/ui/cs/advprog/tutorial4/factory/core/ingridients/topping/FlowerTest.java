package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlowerTest {
    Flower flower;

    @BeforeEach
    public void setup() throws Exception {
        flower = new Flower();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Xinqin Flower Topping...";
        assertEquals(result, flower.getDescription());
    }
}
