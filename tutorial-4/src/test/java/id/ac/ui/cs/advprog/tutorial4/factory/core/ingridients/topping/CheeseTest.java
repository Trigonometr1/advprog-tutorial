package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    Cheese cheese;

    @BeforeEach
    public void setup() throws Exception {
        cheese = new Cheese();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Shredded Cheese Topping...";
        assertEquals(result, cheese.getDescription());
    }
}
