package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PorkTest {
    Pork pork;

    @BeforeEach
    public void setup() throws Exception {
        pork = new Pork();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Tian Xu Pork Meat...";
        assertEquals(result, pork.getDescription());
    }
}
