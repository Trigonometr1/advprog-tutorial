package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiratakiTest {
    Shirataki shirataki;

    @BeforeEach
    public void setup() throws Exception {
        shirataki = new Shirataki();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Snevnezha Shirataki Noodles...";
        assertEquals(result, shirataki.getDescription());
    }
}
