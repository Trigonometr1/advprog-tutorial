package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private MondoUdon mondoUdon;
    private final String NAME = "Test Mondo Udon";

    @BeforeEach
    public void setup() throws Exception {
        mondoUdon = new MondoUdon(NAME);
    }

    @Test
    public void testGetNameMethod() {
        assertEquals(NAME, mondoUdon.getName());
    }

    @Test
    public void testGetNoodleReturnCorrectInstance() {
        Noodle noodle = mondoUdon.getNoodle();
        assertTrue(noodle instanceof Udon);
    }

    @Test
    public void testGetMeatReturnCorrectInstance() {
        Meat meat = mondoUdon.getMeat();
        assertTrue(meat instanceof Chicken);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        Topping topping = mondoUdon.getTopping();
        assertTrue(topping instanceof Cheese);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        Flavor flavor = mondoUdon.getFlavor();
        assertTrue(flavor instanceof Salty);
    }
}
