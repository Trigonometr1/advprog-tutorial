package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RamenTest {
    Ramen ramen;

    @BeforeEach
    public void setup() throws Exception {
        ramen = new Ramen();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Inuzuma Ramen Noodles...";
        assertEquals(result, ramen.getDescription());
    }
}
