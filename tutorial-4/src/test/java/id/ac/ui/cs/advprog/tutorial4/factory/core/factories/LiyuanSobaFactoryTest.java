package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaFactoryTest {
    LiyuanSobaFactory lisofaObject;

    @BeforeEach
    public void setup() throws Exception {
        lisofaObject = new LiyuanSobaFactory();
    }

    @Test
    public void testGetFlavorMethod() {
        Flavor flavor = lisofaObject.getFlavor();
        assertTrue(flavor instanceof Sweet);
    }

    @Test
    public void testGetMeatMethod() {
        Meat meat = lisofaObject.getMeat();
        assertTrue(meat instanceof Beef);
    }

    @Test
    public void testGetNoodleMethod() {
        Noodle noodle = lisofaObject.getNoodle();
        assertTrue(noodle instanceof Soba);
    }

    @Test
    public void testGetToppingMethod() {
        Topping topping = lisofaObject.getTopping();
        assertTrue(topping instanceof Mushroom);
    }
}
