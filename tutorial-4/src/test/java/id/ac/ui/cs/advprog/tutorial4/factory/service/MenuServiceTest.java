package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    private Class<?> menuServiceClass;
    private MenuService menuService;

    @BeforeEach
    public void setup() throws Exception {
        menuService = new MenuServiceImpl();
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService");
    }

    @Test
    public void testGetMenusMethod() {
        assertEquals(4, menuService.getMenus().size());
    }
}
