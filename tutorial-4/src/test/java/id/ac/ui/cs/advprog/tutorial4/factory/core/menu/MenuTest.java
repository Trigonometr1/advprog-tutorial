package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.LiyuanSobaFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuTest {
    private Menu menu;

    @BeforeEach
    public void setup() throws Exception {
        menu = new Menu("test", new LiyuanSobaFactory());
    }

    @Test
    public void testGetNameMethod() {
        String name = "test";
        assertEquals(name, menu.getName());
    }

    @Test
    public void testGetNoodleMethod() {
        String noodle = "Adding Liyuan Soba Noodles...";
        assertEquals(noodle, menu.getNoodle().getDescription());
    }

    @Test
    public void testGetMeatMethod() {
        String meat = "Adding Maro Beef Meat...";
        assertEquals(meat, menu.getMeat().getDescription());
    }

    @Test
    public void testGetToppingMethod() {
        String topping = "Adding Shiitake Mushroom Topping...";
        assertEquals(topping, menu.getTopping().getDescription());
    }

    @Test
    public void testGetFlavorMethod() {
        String flavor = "Adding a dash of Sweet Soy Sauce...";
        assertEquals(flavor, menu.getFlavor().getDescription());
    }
}
