package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenFactoryTest {
    InuzumaRamenFactory irafaObject;

    @BeforeEach
    public void setup() throws Exception {
        irafaObject = new InuzumaRamenFactory();
    }

    @Test
    public void testGetFlavorMethod() {
        Flavor irafaFlavor = irafaObject.getFlavor();
        assertTrue(irafaFlavor instanceof Spicy);
    }

    @Test
    public void testGetMeatMethod() {
        Meat irafaFlavor = irafaObject.getMeat();
        assertTrue(irafaFlavor instanceof Pork);
    }

    @Test
    public void testGetNoodleMethod() {
        Noodle irafaFlavor = irafaObject.getNoodle();
        assertTrue(irafaFlavor instanceof Ramen);
    }

    @Test
    public void testGetToppingMethod() {
        Topping irafaFlavor = irafaObject.getTopping();
        assertTrue(irafaFlavor instanceof BoiledEgg);
    }
}
