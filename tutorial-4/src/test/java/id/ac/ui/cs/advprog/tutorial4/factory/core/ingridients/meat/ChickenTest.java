package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {
    Chicken chicken;

    @BeforeEach
    public void setup() throws Exception {
        chicken = new Chicken();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Wintervale Chicken Meat...";
        assertEquals(result, chicken.getDescription());
    }
}
