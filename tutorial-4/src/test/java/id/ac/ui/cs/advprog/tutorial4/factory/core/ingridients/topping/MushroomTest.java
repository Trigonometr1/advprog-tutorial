package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MushroomTest {
    Mushroom mushroom;

    @BeforeEach
    public void setup() throws Exception {
        mushroom = new Mushroom();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Shiitake Mushroom Topping...";
        assertEquals(result, mushroom.getDescription());
    }
}
