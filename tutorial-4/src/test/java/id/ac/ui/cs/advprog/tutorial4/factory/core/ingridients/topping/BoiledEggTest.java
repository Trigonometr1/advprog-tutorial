package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {
    BoiledEgg boiledEgg;

    @BeforeEach
    public void setup() throws Exception {
        boiledEgg = new BoiledEgg();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Guahuan Boiled Egg Topping";
        assertEquals(result, boiledEgg.getDescription());
    }
}
