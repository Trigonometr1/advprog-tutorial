package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeefTest {
    Beef beef;

    @BeforeEach
    public void setup() throws Exception {
        beef = new Beef();
    }

    @Test
    public void testGetDescriptionMethod() {
        String result = "Adding Maro Beef Meat...";
        assertEquals(result, beef.getDescription());
    }
}
