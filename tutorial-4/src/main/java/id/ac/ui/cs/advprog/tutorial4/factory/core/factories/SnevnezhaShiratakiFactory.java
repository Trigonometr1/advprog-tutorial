package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class SnevnezhaShiratakiFactory implements Factory {
    /**
     * Getter Flavor method for CLASSNAME
     *
     * @return Flavor of CLASSNAME
     */
    @Override
    public Flavor getFlavor() {
        return new Umami();
    }

    /**
     * Getter Meat method for CLASSNAME
     *
     * @return Meat of CLASSNAME
     */
    @Override
    public Meat getMeat() {
        return new Fish();
    }

    /**
     * Getter Noodle method for CLASSNAME
     *
     * @return Noodle of CLASSNAME
     */
    @Override
    public Noodle getNoodle() {
        return new Shirataki();
    }

    /**
     * Getter Topping method for CLASSNAME
     *
     * @return Topping of CLASSNAME
     */
    @Override
    public Topping getTopping() {
        return new Flower();
    }
}
