package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class MondoUdonFactory implements Factory {
    /**
     * Getter Flavor method for CLASSNAME
     *
     * @return Flavor of CLASSNAME
     */
    @Override
    public Flavor getFlavor() { return new Salty(); }

    /**
     * Getter Meat method for CLASSNAME
     *
     * @return Meat of CLASSNAME
     */
    @Override
    public Meat getMeat() {
        return new Chicken();
    }

    /**
     * Getter Noodle method for CLASSNAME
     *
     * @return Noodle of CLASSNAME
     */
    @Override
    public Noodle getNoodle() {
        return new Udon();
    }

    /**
     * Getter Topping method for CLASSNAME
     *
     * @return Topping of CLASSNAME
     */
    @Override
    public Topping getTopping() {
        return new Cheese();
    }
}
