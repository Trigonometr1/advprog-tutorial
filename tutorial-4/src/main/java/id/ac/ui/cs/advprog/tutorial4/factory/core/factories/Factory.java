package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public interface Factory {
    /**
     * Getter Flavor method for CLASSNAME
     * @return Flavor of CLASSNAME
     */
    public Flavor getFlavor();

    /**
     * Getter Meat method for CLASSNAME
     * @return Meat of CLASSNAME
     */
    public Meat getMeat();

    /**
     * Getter Noodle method for CLASSNAME
     * @return Noodle of CLASSNAME
     */
    public Noodle getNoodle();

    /**
     * Getter Topping method for CLASSNAME
     * @return Topping of CLASSNAME
     */
    public Topping getTopping();
}
