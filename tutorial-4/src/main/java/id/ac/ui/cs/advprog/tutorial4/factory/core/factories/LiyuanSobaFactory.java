package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class LiyuanSobaFactory implements Factory {
    /**
     * Getter Flavor method for CLASSNAME
     *
     * @return Flavor of CLASSNAME
     */
    @Override
    public Flavor getFlavor() {
        return new Sweet();
    }

    /**
     * Getter Meat method for CLASSNAME
     *
     * @return Meat of CLASSNAME
     */
    @Override
    public Meat getMeat() {
        return new Beef();
    }

    /**
     * Getter Noodle method for CLASSNAME
     *
     * @return Noodle of CLASSNAME
     */
    @Override
    public Noodle getNoodle() {
        return new Soba();
    }

    /**
     * Getter Topping method for CLASSNAME
     *
     * @return Topping of CLASSNAME
     */
    @Override
    public Topping getTopping() {
        return new Mushroom();
    }
}
