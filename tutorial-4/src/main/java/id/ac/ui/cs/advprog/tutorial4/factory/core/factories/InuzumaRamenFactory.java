package id.ac.ui.cs.advprog.tutorial4.factory.core.factories;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class InuzumaRamenFactory implements Factory {
    /**
     * Getter Flavor method for InuzumaRamenFactory
     *
     * @return Flavor of InuzumaRamenFactory
     */
    @Override
    public Flavor getFlavor() {
        return new Spicy();
    }

    /**
     * Getter Meat method for InuzumaRamenFactory
     *
     * @return Meat of InuzumaRamenFactory
     */
    @Override
    public Meat getMeat() {
        return new Pork();
    }

    /**
     * Getter Noodle method for InuzumaRamenFactory
     *
     * @return Noodle of InuzumaRamenFactory
     */
    @Override
    public Noodle getNoodle() {
        return new Ramen();
    }

    /**
     * Getter Topping method for InuzumaRamenFactory
     *
     * @return Topping of InuzumaRamenFactory
     */
    @Override
    public Topping getTopping() {
        return new BoiledEgg();
    }
}
