package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.*;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MyTransformationTest {
    private Class<?> myTransClass;
    private MyTransformation myTransObject;
    private  MyTransformation myTransObjectWithShift;

    @BeforeEach
    public void setup() throws Exception {
        myTransClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.MyTransformation");
        myTransObject = new MyTransformation();
        myTransObjectWithShift = new MyTransformation(3);
    }

    @Test
    public void testMyTransHasEncodeMethod() throws Exception {
        Method translate = myTransClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTransEncodesCorrectly() {
        String text = "Testing";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Uftujoh";
        Spell result = myTransObject.encode(spell);

        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTransEncodesCorrectlyWithShift() {
        String text = "Testing";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Whvwlqj";
        Spell result = myTransObjectWithShift.encode(spell);

        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTransHasDecodeMethod() throws Exception {
        Method translate = myTransClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTransDecodesCorrectly() {
        String text = "Uftujoh";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Testing";
        Spell result = myTransObject.decode(spell);

        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTransDecodesCorrectlyWithShift() {
        String text = "Whvwlqj";
        Codex codex = RunicCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Testing";
        Spell result = myTransObjectWithShift.decode(spell);

        assertEquals(expected, result.getText());
    }
}
