package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isChargedAttackCalledOnce = false;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
    }

    /**
     * Memanggil method smallSpell di SpellBook
     * @return String deskripsinya
     */
    @Override
    public String normalAttack() {
        return String.format("%s attacked with %s (normal attack): %s",
                this.getHolderName(), this.getName(), this.spellbook.smallSpell());
    }

    /**
     * Charged attack SpellBook tidak bisa dua kali berurutan
     * Perlu antisipasi dengan atribut tambahan
     * @return String deskripsi
     */
    @Override
    public String chargedAttack() {
        if(this.isChargedAttackCalledOnce) {
            this.isChargedAttackCalledOnce = false;
            return String.format("%s attacked with %s (charged attack): " +
                            "Magic power not enough for large spell",
                            this.getHolderName(), this.getName());
        } else {
            this.isChargedAttackCalledOnce = true;
            return String.format("%s attacked with %s (charged attack): %s",
                    this.getHolderName(), this.getName(), this.spellbook.largeSpell());
        }
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
