package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// TODO: complete me :)
public class BowAdapter implements Weapon {

    private Bow bow;
    // Kalo false, tandanya lagi normal mode
    private boolean isAimShot = false;


    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    /**
     * Melakukan method shootArrow di bow
     * tergantung kondisi isAimShot di class ini
     * @return String deskripsi hasil return shootArrow bow
     */
    @Override
    public String normalAttack() {
        return String.format("%s attacked with %s (normal attack): %s",
                this.getHolderName(), this.getName(), bow.shootArrow(isAimShot));
    }

    /**
     * Akan merubah isAimMode serta memberitahu perubahannya
     * @return String yang memberitahu perubahan aim mode
     */
    @Override
    public String chargedAttack() {
        if (this.isAimShot) {
            this.isAimShot = false;
            return String.format("%s attacked with %s (charged attack): " +
                            "Leaving aim shot mode", this.getHolderName(),
                            this.getName());
        } else {
            this.isAimShot = true;
            return String.format("%s attacked with %s (charged attack): " +
                            "Entering aim shot mode", this.getHolderName(),
                            this.getName());
        }
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return this.bow.getHolderName();
    }
}
