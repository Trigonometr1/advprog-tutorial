package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class SeawardPride implements Weapon {


    private String holderName;

    public SeawardPride(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        // TODO: complete me
        return String.format("%s attacked with %s (normal attack): " +
                        "Life-destroyer Pride!!", this.getHolderName(),
                        this.getName());
    }

    @Override
    public String chargedAttack() {
        // TODO: complete me
        return String.format("%s attacked with %s (charged attack): " +
                        "The Sin of Pride consumes you!", this.getHolderName(),
                        this.getName());
    }

    @Override
    public String getName() {
        return "Seaward Pride";
    }

    @Override
    public String getHolderName() { return holderName; }
}
