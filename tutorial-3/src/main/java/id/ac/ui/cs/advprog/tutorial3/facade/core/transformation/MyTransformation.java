package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class MyTransformation implements Transformation {
    private int shift;

    /**
     * Constructor for desired jump
     *
     * @param shift how far you want to swap the char
     */
    public MyTransformation(int shift) {
        this.shift = shift;
    }

    /**
     * Constructor if you want the default setting
     */
    public MyTransformation() {
        this(1);
    }

    /**
     * Encode the Spell given based on the codex
     *
     * @param spell that want to be encoded
     * @return encoded spell
     */
    @Override
    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    /**
     * Decode the Spell given based on the codex
     *
     * @param spell that want to be decoded
     * @return decoded spell
     */
    @Override
    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    /**
     * Core method for encode and decode
     * @param spell spell that have been encoded or decoded
     * @param encode decode if false, encode if true
     * @return encoded or decoded spell
     */
    private Spell process(Spell spell, boolean encode) {
        // Kolaborator: Steven - 1906293322
        StringBuilder result = new StringBuilder();

        int jumpAmt = encode ? shift : -1 * shift;

        for (char ch : spell.getText().toCharArray()) {
            result.append(shiftToChar(ch, jumpAmt));
        }

        return new Spell(result.toString(), spell.getCodex());

    }


    /**
     * Helper method to take the encoded/decoded character
     * @param ch character that want to be encoded/decoded
     * @param shiftTo how far you want to swap the char
     * @return encoded/decoded character
     */
    private char shiftToChar(char ch, int shiftTo) {
        return (char) (ch + shiftTo);
    }
}
