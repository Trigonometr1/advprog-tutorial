package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public interface Transformation {
    /**
     * Encode the Spell given based on the codex
     * @param spell that want to be encoded
     * @return encoded spell
     */
    public Spell encode(Spell spell);

    /**
     * Decode the Spell given based on the codex
     * @param spell that want to be decoded
     * @return decoded spell
     */
    public Spell decode(Spell spell);
}
