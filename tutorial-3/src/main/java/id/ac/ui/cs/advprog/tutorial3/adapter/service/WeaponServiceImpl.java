package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    private boolean isFirstCall = true;


    // TODO: implement me
    /**
     * Make all Bow and Spellbook to Weapon,
     * then store it in weaponRepository
     * @return List as returned in weaponRepository.findAll()
     */
    @Override
    public List<Weapon> findAll() {
        // Buat semua Bow tanpa Adapter ke Weapon
        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null)
                weaponRepository.save(new BowAdapter(bow));
        }
        // Buat semua Spellbook tanpa Adapter ke Weapon
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null)
                weaponRepository.save(new SpellbookAdapter(spellbook));
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);

        if (attackType == 2)
            logRepository.addLog(weapon.chargedAttack());
        else
            logRepository.addLog(weapon.normalAttack());

        this.weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return this.logRepository.findAll();
    }
}
