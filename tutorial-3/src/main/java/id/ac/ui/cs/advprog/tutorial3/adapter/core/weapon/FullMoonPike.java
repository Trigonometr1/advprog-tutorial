package id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon;

public class FullMoonPike implements Weapon {

    private String holderName;

    public FullMoonPike(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public String normalAttack() {
        // TODO: complete me
        return String.format("%s attacked with %s (normal attack): " +
                        "Witness the sorcery of Full Moon", this.getHolderName(),
                        this.getName());
    }

    @Override
    public String chargedAttack() {
        // TODO: complete me
        return String.format("%s attacked with %s (charged attack): " +
                        "Full Moon Arts shall hinder thou!", this.getHolderName(),
                        this.getName());
    }

    @Override
    public String getName() {
        return "Full Moon Pike";
    }

    @Override
    public String getHolderName() { return holderName; }
}
